use self::{ActionError::*, Continuation::*};
use git2::Repository;
use serde::{Deserialize, Serialize};
use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    io::{self, BufRead, Write},
    process::Command,
};

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(untagged)]
pub enum Action {
    Special(SpecialAction),
    Command(String),
}

impl Action {
    pub fn apply(&self, repo: &Repository) -> Result<ActionResult, ActionError> {
        match self {
            Action::Special(special) => special.apply(repo),
            Action::Command(cmd) => {
                let status = Command::new("sh")
                    .arg("-c")
                    .arg(cmd)
                    .status()
                    .map_err(|e| ExecError(e.to_string()))?;
                if status.success() {
                    Ok(ActionResult::new(Continue, None))
                } else {
                    Err(ExitStatus)
                }
            }
        }
    }
}

impl Display for Action {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Action::Special(action) => write!(f, "{}", action),
            Action::Command(cmd) => write!(f, "{}", cmd),
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum SpecialAction {
    Pause,
    Continue,
    Optional(Box<Action>),
}

impl SpecialAction {
    pub fn apply(&self, repo: &Repository) -> Result<ActionResult, ActionError> {
        match self {
            SpecialAction::Pause => {
                println!("Press enter to continue ...");
                let _ = io::stdin().lock().lines().next();
                Ok(ActionResult::new(Continue, None))
            }
            SpecialAction::Continue => {
                print!("Continue? (y)es, (N)o, (a)bort ");
                io::stdout().flush().unwrap();
                let mut continuation = Break;
                for line in io::stdin().lock().lines() {
                    match line
                        .map_err(|e| IoError(e.to_string()))?
                        .trim()
                        .to_lowercase()
                        .chars()
                        .next()
                    {
                        Some('y') => continuation = Continue,
                        Some('n') | None => {}
                        Some('a') => continuation = Abort,
                        _ => continue,
                    }
                    break;
                }
                Ok(ActionResult::new(continuation, None))
            }
            SpecialAction::Optional(action) => {
                let msg = action.apply(repo).err().map(|x| x.to_string());
                Ok(ActionResult::new(Continue, msg))
            }
        }
    }
}

impl Display for SpecialAction {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            SpecialAction::Pause => write!(f, "pause"),
            SpecialAction::Continue => write!(f, "continue"),
            SpecialAction::Optional(action) => write!(f, "optional: {}", action),
        }
    }
}

#[derive(Debug)]
pub struct ActionResult {
    pub continuation: Continuation,
    pub message: Option<String>,
}

impl ActionResult {
    pub fn new(continuation: Continuation, message: Option<String>) -> Self {
        ActionResult {
            continuation,
            message,
        }
    }
}

#[derive(Debug)]
pub enum Continuation {
    Break,
    Continue,
    Abort,
}

#[derive(Debug, Clone)]
pub enum ActionError {
    IoError(String),
    ExecError(String),
    ExitStatus,
}

impl Display for ActionError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            ActionError::IoError(msg) => write!(f, "IO error: {}", msg),
            ActionError::ExecError(msg) => write!(f, "Could not run command: {}", msg),
            ActionError::ExitStatus => f.write_str("Command returned non-zero status."),
        }
    }
}

impl Error for ActionError {}
