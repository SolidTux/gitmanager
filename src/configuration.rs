use crate::action::Action;
use git2::{Repository, RepositoryState, Status};
use regex::Regex;
use serde::Deserialize;
use std::{error::Error, path::PathBuf};

#[derive(Debug, Deserialize, Default, Clone)]
#[serde(default)]
pub struct Config {
    pub folders: Vec<PathBuf>,
    pub filters: Vec<FilterRule>,
    pub actions: Vec<Action>,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum FilterRule {
    Remote(String),
    RemoteUrl(String),
    #[serde(with = "RepositoryStateDef")]
    State(RepositoryState),
    Status(StatusRule),
    Any(Vec<FilterRule>),
}

impl FilterRule {
    pub fn matches(&self, repo: &Repository) -> Result<bool, Box<dyn Error>> {
        match self {
            FilterRule::Remote(s) => {
                let re = Regex::new(s)?;
                Ok(repo
                    .remotes()?
                    .iter()
                    .flatten()
                    .any(|remote| re.is_match(remote)))
            }
            FilterRule::RemoteUrl(s) => {
                let re = Regex::new(s)?;
                Ok(repo
                    .remotes()?
                    .iter()
                    .flatten()
                    .filter_map(|remote| repo.find_remote(remote).ok())
                    .any(|remote| remote.url().map(|url| re.is_match(url)).unwrap_or(false)))
            }
            FilterRule::State(state) => Ok(&repo.state() == state),
            FilterRule::Status(rule) => Ok(repo
                .statuses(None)?
                .iter()
                .map(|x| x.status())
                .any(|status| rule.matches(&status))),
            FilterRule::Any(rules) => {
                let mut matches = false;
                for rule in rules {
                    matches |= rule.matches(repo)?;
                }
                Ok(matches)
            }
        }
    }
}

#[derive(Deserialize)]
#[serde(remote = "RepositoryState", rename_all = "snake_case")]
pub enum RepositoryStateDef {
    Clean,
    Merge,
    Revert,
    RevertSequence,
    CherryPick,
    CherryPickSequence,
    Bisect,
    Rebase,
    RebaseInteractive,
    RebaseMerge,
    ApplyMailbox,
    ApplyMailboxOrRebase,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum StatusRule {
    Conflicted,
    Empty,
    Ignored,
    IndexDeleted,
    IndexModified,
    IndexNew,
    IndexRenamed,
    IndexTypeChange,
    Deleted,
    Modified,
    New,
    Renamed,
    TypeChange,
}

impl StatusRule {
    pub fn matches(&self, status: &Status) -> bool {
        match self {
            StatusRule::Conflicted => status.is_conflicted(),
            StatusRule::Empty => status.is_empty(),
            StatusRule::Ignored => status.is_ignored(),
            StatusRule::IndexDeleted => status.is_index_deleted(),
            StatusRule::IndexModified => status.is_index_modified(),
            StatusRule::IndexNew => status.is_index_new(),
            StatusRule::IndexRenamed => status.is_index_renamed(),
            StatusRule::IndexTypeChange => status.is_index_typechange(),
            StatusRule::Deleted => status.is_wt_deleted(),
            StatusRule::Modified => status.is_wt_modified(),
            StatusRule::New => status.is_wt_new(),
            StatusRule::Renamed => status.is_wt_renamed(),
            StatusRule::TypeChange => status.is_wt_typechange(),
        }
    }
}
