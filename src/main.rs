mod action;
mod configuration;

use self::ApplicationError::*;
use crate::{action::Continuation, configuration::Config};
use ansi_term::{Color, Style};
use config::ConfigError;
use directories::{ProjectDirs, UserDirs};
use git2::Repository;
use shellexpand::path::LookupError;
use std::{
    collections::HashMap,
    env::{self, VarError},
    error::Error,
    fmt::{self, Debug, Display, Formatter},
};
use structopt::StructOpt;
use walkdir::WalkDir;

#[derive(Debug, StructOpt)]
#[structopt(about, author)]
struct Opt {
    /// Name of configuration
    config: Option<String>,
}

enum ApplicationError {
    Dir(String),
    Config(String),
    Filter(String),
    Action(String, String),
}

impl Error for ApplicationError {}

impl Display for ApplicationError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let s = match self {
            Dir(msg) => format!("Directory error: {}", msg),
            Config(msg) => format!("Error with configuration: {}", msg),
            Filter(msg) => format!("Error while evaluating filter: {}", msg),
            Action(action, msg) => format!("Error while executing action {}: {}", action, msg),
        };
        write!(f, "{}", Style::default().bold().fg(Color::Red).paint(s))
    }
}

impl Debug for ApplicationError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl From<LookupError<VarError>> for ApplicationError {
    fn from(value: LookupError<VarError>) -> Self {
        ApplicationError::Dir(value.to_string())
    }
}

impl From<ConfigError> for ApplicationError {
    fn from(value: ConfigError) -> Self {
        ApplicationError::Config(value.to_string())
    }
}

fn main() -> Result<(), ApplicationError> {
    let opt = Opt::from_args();
    let config_path = {
        let project_dirs = ProjectDirs::from("de", "solidtux", "gitmanager").unwrap();
        let mut path = project_dirs.config_dir().to_path_buf();
        path.push("config");
        path
    };
    let configs: HashMap<String, Config> = {
        let builder = config::Config::builder()
            .add_source(config::File::with_name(config_path.to_str().unwrap()))
            .build()
            .unwrap();
        builder.try_deserialize()?
    };
    let config = match opt.config {
        Some(name) => configs
            .get(&name)
            .ok_or(Config(format!("Configuration {} not found.", name)))?
            .clone(),
        None => match configs.get("default") {
            Some(entry) => entry.clone(),
            None => configs.values().next().cloned().unwrap_or_default(),
        },
    };
    let folders = if config.folders.is_empty() {
        vec![UserDirs::new().unwrap().home_dir().to_path_buf()]
    } else {
        config.folders.clone()
    };
    for folder in folders {
        let expanded_folder = shellexpand::path::full(&folder)?;
        'entries: for entry in WalkDir::new(expanded_folder)
            .into_iter()
            .filter_entry(|e| e.file_type().is_dir())
        {
            let e = entry.map_err(|e| Dir(e.to_string()))?;
            if e.file_name()
                .to_str()
                .map(|x| x.to_lowercase() == ".git")
                .unwrap_or(false)
            {
                let mut path = e.into_path();
                path.pop();
                env::set_current_dir(&path).map_err(|e| Dir(e.to_string()))?;
                if let Ok(repo) = Repository::open(&path) {
                    let mut matches = true;
                    for filter in &config.filters {
                        matches &= filter.matches(&repo).map_err(|e| Filter(e.to_string()))?;
                    }
                    if matches {
                        println!(
                            "{}",
                            Style::default()
                                .bold()
                                .fg(Color::Green)
                                .paint(path.into_os_string().into_string().unwrap())
                        );
                        for action in &config.actions {
                            let res = action.apply(&repo).map_err(|e| {
                                ApplicationError::Action(action.to_string(), e.to_string())
                            })?;
                            let mut style = Style::new().bold();
                            match res.continuation {
                                Continuation::Continue => style = style.fg(Color::Green),
                                Continuation::Break => style = style.fg(Color::Yellow),
                                Continuation::Abort => style = style.fg(Color::Red),
                            }
                            if let Some(msg) = res.message {
                                println!("{}", style.paint(msg));
                            }
                            match res.continuation {
                                Continuation::Continue => {}
                                Continuation::Break => continue 'entries,
                                Continuation::Abort => return Ok(()),
                            }
                        }
                    }
                }
            }
        }
    }
    Ok(())
}
